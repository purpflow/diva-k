use core::intrinsics::volatile_load;
use core::intrinsics::volatile_store;

pub const MMIO_BASE: u32 = 0x3F000000; // RPI 3 | 4

pub fn write(reg: u32, val: u32) {
    unsafe { volatile_store(reg as *mut u32, val) }
}
pub fn read(reg: u32) -> u32 {
    unsafe { volatile_load(reg as *const u32) }
}