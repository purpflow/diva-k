use crate::io::{mmio, uart0};

fn transmit_fifo_full() -> bool {
    mmio::read(uart0::FR) & (1 << 5) > 0
}

fn receive_fifo_empty() -> bool {
    mmio::read(uart0::FR) & (1 << 4) > 0
}

pub fn putc(c: u8) {
    while transmit_fifo_full() {}
    mmio::write(uart0::DR, c as u32);
}

pub fn getc() -> u8 {
    while receive_fifo_empty() {}
    mmio::read(uart0::DR) as u8
}

pub fn puts(msg: &str) {
    for c in msg.chars() {
        putc(c as u8)
    }
}

pub fn hex(d: u16) {
    // TODO
}