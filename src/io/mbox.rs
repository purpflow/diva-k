use core::arch::asm;
use core::fmt::Error;
use crate::io::mmio;
use crate::io::uart0::serial;

pub const REQUEST: i32 = 0;

/* channels */
pub const CH_POWER: u8 = 0;
pub const CH_FB: u8    = 1;
pub const CH_VUART: u8 = 2;
pub const CH_VCHIQ: u8 = 3;
pub const CH_LEDS: u8  = 4;
pub const CH_BTNS: u8  = 5;
pub const CH_TOUCH: u8 = 6;
pub const CH_COUNT: u8 = 7;
pub const CH_PROP: u8  = 8;

/* tags */
pub const TAG_SETPOWER: i32      = 0x28001;
pub const TAG_GETSERIAL: i32     = 0x10004;
pub const TAG_SETCLKRATE: i32    = 0x38002;
pub const TAG_LAST: i32          = 0;


const VIDEOCORE_MBOX: u32 = mmio::MMIO_BASE + 0x0000B880;
const MBOX_READ: u32      = VIDEOCORE_MBOX+0x0;
const MBOX_POLL: u32      = VIDEOCORE_MBOX+0x10;
const MBOX_SENDER: u32    = VIDEOCORE_MBOX+0x14;
const MBOX_STATUS: u32    = VIDEOCORE_MBOX+0x18;
const MBOX_CONFIG: u32    = VIDEOCORE_MBOX+0x1C;
const MBOX_WRITE: u32     = VIDEOCORE_MBOX+0x20;
const MBOX_RESPONSE: u32  = 0x80000000;
const MBOX_FULL: u32      = 0x80000000;
const MBOX_EMPTY: u32     = 0x40000000;



// volatile unsigned int  __attribute__((aligned(16))) MBOX[36];

pub static mut MBOX: [i32; 32]  = [0; 32];

pub unsafe fn mbox_call(ch: u8) -> Result<(), Error> {
    // unsigned int r = (((unsigned int)((unsigned long)&MBOX)&~0xF) | (ch&0xF));
    let r: u32 = ((MBOX.as_ptr() as u64) & !0xF) as u32 | (ch & 0xF) as u32;

    while mmio::read(MBOX_STATUS) & MBOX_FULL != 0 {
        asm!("nop");
    };

    mmio::write(MBOX_WRITE, r);

    loop {
        while mmio::read(MBOX_STATUS) & MBOX_EMPTY != 0 {
            asm!("nop");
        };

        return if r == mmio::read(MBOX_READ) &&
            MBOX[1] == MBOX_RESPONSE as i32 {
            Ok(())
        } else {
            Err(Error)
        }
    }
}