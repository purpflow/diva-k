use core::arch::asm;
use core::fmt::Error;
use crate::io::{
    mmio,
    mbox,
    gpio,
};

pub mod serial;

pub const DR: u32 =   mmio::MMIO_BASE + 0x00201000;
pub const FR: u32 =   mmio::MMIO_BASE + 0x00201018;
pub const IBRD: u32 = mmio::MMIO_BASE + 0x00201024;
pub const FBRD: u32 = mmio::MMIO_BASE + 0x00201028;
pub const LCRH: u32 = mmio::MMIO_BASE + 0x0020102C;
pub const CR: u32 =   mmio::MMIO_BASE + 0x00201030;
pub const IMSC: u32 = mmio::MMIO_BASE + 0x00201038;
pub const ICR: u32 =  mmio::MMIO_BASE + 0x00201044;

pub fn init() -> Result<(), Error> {
    let mut r: u32;

    mmio::write(CR, 0); // turn off UART0

    /* set up clock for consistent divisor values */
    unsafe {
        mbox::MBOX[0] = 9 * 4;
        mbox::MBOX[1] = mbox::REQUEST;
        mbox::MBOX[2] = mbox::TAG_SETCLKRATE; // set clock rate
        mbox::MBOX[3] = 12;
        mbox::MBOX[4] = 8;
        mbox::MBOX[5] = 2;           // UART clock
        mbox::MBOX[6] = 4000000;     // 4Mhz
        mbox::MBOX[7] = 0;           // clear turbo
        mbox::MBOX[8] = mbox::TAG_LAST;
        match mbox::mbox_call(mbox::CH_PROP) {
            Ok(_) => {}
            Err(e) => { return Err(e) }
        }
    }

    /* map UART0 to GPIO pins */
    r = mmio::read(gpio::FSEL1);
    r &= !((7 << 12) | (7 << 15)); // gpio14, gpio15
    r |= (4 << 12) | (4 << 15); // alt0
    mmio::write(gpio::FSEL1, r);
    mmio::write(gpio::PUD, 0);   // enable pins 14 and 15
    for _ in (0..150).rev() {
        unsafe { asm!("nop"); }
    }
    mmio::write(gpio::PUDCLK0, (1 << 14) | (1 << 15));
    for _ in (0..150).rev() {
        unsafe { asm!("nop"); }
    }
    mmio::write(gpio::PUDCLK0, 0);

    mmio::write(ICR, 0x7FF);     // clear interrupts
    mmio::write(IBRD, 2);        // 115200 baud
    mmio::write(FBRD, 0xB);
    mmio::write(LCRH, 0x7 << 4); // 8n1, enable FIFOs
    mmio::write(CR, 0x301);      // enable Tx, Rx, UART

    Ok(())
}
