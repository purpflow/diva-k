use core::fmt::Error;

pub mod uart0;
mod gpio;
mod mmio;
mod mbox;


pub fn init() -> Result<(), Error> {
    uart0::serial::puts("UART0 init...");
    match uart0::init() {
        Ok(_) => {uart0::serial::puts("[Ok]\n");}
        Err(_) => {uart0::serial::puts("[Err]\n")}
    };
    Ok(())
}
