use core::fmt::Error;

mod mmu;

pub fn init() -> Result<(), Error> {
    mmu::init()
}