#![no_std]
#![feature(core_intrinsics, lang_items)]

use aarch64_cpu::asm;

use core::intrinsics::abort;
use core::panic::PanicInfo;

mod io;
mod cpu;

fn hlt_loop() {
    loop { asm::wfe() }
}

#[no_mangle]
pub extern fn kernel_main() {
    io::uart0::serial::puts("[IO init]\n");
    io::init();
    hlt_loop();
}


#[no_mangle]
pub extern fn exc_handler() {
    io::uart0::serial::puts("Kernel exception!\n");
    hlt_loop()
}


// what??
#[no_mangle]
pub extern fn _ZN4core9panicking5panic17hb3eeba97a885a01eE() {
    io::uart0::serial::puts("Kernel panic!\n");
    hlt_loop();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    io::uart0::serial::puts("Kernel panic!\n");
    unsafe { abort() }
}

