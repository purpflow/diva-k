CC_DIR = $(HOME)/opt/cross/bin
RC = xargo
TARGET_NAME = aarch64-unknown-none
TARGET_DIR = target/$(TARGET_NAME)

all: kernel-aarch64.elf

rust:
	$(RC) build --target $(TARGET_NAME)

$(TARGET_DIR)/boot.o: src/bootload/boot-aarch64.S
	$(CC_DIR)/aarch64-none-elf-gcc $(CFLAGS) \
	 -c src/bootload/boot-aarch64.S -o $(TARGET_DIR)/boot.o

kernel-aarch64.elf: rust $(TARGET_DIR)/boot.o
	$(CC_DIR)/aarch64-none-elf-gcc -T linkers/aarch64.ld \
 -o $(TARGET_DIR)/debug/kernel-aarch64.elf \
 -ffreestanding -O2 -nostdlib $(TARGET_DIR)/boot.o $(TARGET_DIR)/debug/libdiva_k.rlib


run:
	qemu-system-aarch64 -M raspi3b \
	-kernel $(TARGET_DIR)/debug/kernel-aarch64.elf \
	-serial stdio